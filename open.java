//首先设置两边的退出动画
//translate_left.xml
<?xml version="1.0" encoding="UTF-8"?>
<set android:fillAfter="false"
  xmlns:android="http://schemas.android.com/apk/res/android">
    <translate 
        android:duration="500" 
        android:fromXDelta="0.0" 
        android:toXDelta="-50.0%p" 
        android:fromYDelta="0.0" 
        android:toYDelta="0.0" />
</set>

//translate_right.xml
<?xml version="1.0" encoding="UTF-8"?>
<set xmlns:android="http://schemas.android.com/apk/res/android"
    android:fillAfter="false" >
    <translate
        android:duration="500"
        android:fromXDelta="0.0"
        android:fromYDelta="0.0"
        android:toXDelta="50.0%p"
        android:toYDelta="0.0" />
</set>

//在Activity的布局中用两个linearlayout作为左门和右门
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:orientation="horizontal"
    android:id="@+id/main_layout"
    android:layout_width="match_parent"
    android:layout_height="match_parent" >        
        <LinearLayout android:id="@+id/left_layout"
            android:orientation="vertical"
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            android:background="#ff00aa"
            android:layout_weight="0.4" >      
            <TextView android:id="@+id/lefttextview"
                android:layout_width="match_parent"
                android:layout_height="match_parent"
                android:text="left door" />         
        </LinearLayout>   
        <LinearLayout android:id="@+id/right_layout"
            android:orientation="vertical"
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            android:background="#aa00ff"
            android:layout_weight="0.4" >            
            <TextView android:id="@+id/righttextview"
                android:layout_width="match_parent"
                android:layout_height="match_parent"
                android:text="right door" />    
        </LinearLayout>     
        <Button android:id="@+id/opendoor"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="Open" />
</LinearLayout>

//在代码中实现退出动画效果
public class MainActivity extends Activity {
	private Button open;
	private LinearLayout mainLayout;
	private LinearLayout leftLayout;
	private LinearLayout rightLayout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		open = (Button) findViewById(R.id.opendoor);
		mainLayout = (LinearLayout) findViewById(R.id.main_layout);
		leftLayout = (LinearLayout) findViewById(R.id.left_layout);
		rightLayout = (LinearLayout) findViewById(R.id.right_layout);
		open.setOnClickListener(new Button.OnClickListener(){
			@Override
			public void onClick(View v) {
				open.setVisibility(View.GONE);
				Animation leftOutAnimation = AnimationUtils.loadAnimation(getApplicationContext(),
						R.anim.translate_left);
				Animation rightOutAnimation = AnimationUtils.loadAnimation(getApplicationContext(),
						R.anim.translate_right);
				leftLayout.setAnimation(leftOutAnimation);
				rightLayout.setAnimation(rightOutAnimation);		
				leftOutAnimation.setAnimationListener(new AnimationListener() {				
					@Override
					public void onAnimationStart(Animation animation) {						
					}			
					@Override
					public void onAnimationRepeat(Animation animation) {						
					}
					@Override
					public void onAnimationEnd(Animation animation) {
						leftLayout.setVisibility(View.GONE);
						rightLayout.setVisibility(View.GONE);
						finish();
					}
				});
			}
		});
	}
}